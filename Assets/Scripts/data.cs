﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Data
{
    public static Vector3 position;
    public static bool firstChallengeAccepted = false;
    public static bool secondChallengeAccepted = false;
    public static bool thirdChallengeAccepted = false;
    public static bool sceneChange = false;

    public static bool firstChallengeCompleted = false;
    public static bool secondChallengeCompleted = false;
    public static bool thirdChallengeCompleted = false;

    public static int playerGold;

    public static bool boat1Owned = false, boat2Owned = false, boat3Owned = false, 
        boat4Owned = false, boat5Owned = false, boat6Owned = false;

    public static Sprite boatSkin = null;
}
