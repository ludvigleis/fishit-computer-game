﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StopSound : MonoBehaviour {

    private bool muted;
    public AudioClipGroup music;
    public GameObject soundOffButton;
    public GameObject soundOnButton;


    void Start()
    {
        music.Play();
        soundOnButton.SetActive(false);


    }

    
    public void EnableAudio()
    {
        SetAudioMute(false);
        soundOffButton.SetActive(true);
        soundOnButton.SetActive(false);

    }

    public void DisableAudio()
    {


        SetAudioMute(true);
    
        soundOnButton.SetActive(true);
        soundOffButton.SetActive(false);

    }

 

    private void SetAudioMute(bool mute)
    {
        AudioSource[] sources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
        for (int index = 0; index < sources.Length; ++index)
        {
            sources[index].mute = mute;
        }
        muted = mute;
    }
   
}
