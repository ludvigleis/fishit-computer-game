﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TypeWriterEffect : MonoBehaviour {

	public float delay = 0.1f;
    public float timedelay = 2f;
    public float textpause = 2f;
	public string fullText;
	private string currentText = "";
    public bool isButton = false;
    public GameObject button;

	// Use this for initialization
	void Start () {
        
		StartCoroutine(ShowText());
	}
	
	IEnumerator ShowText(){
        yield return new WaitForSeconds(timedelay);
        for (int i = 0; i <= fullText.Length; i++){
			currentText = fullText.Substring(0,i);
			this.GetComponent<Text>().text = currentText;
			yield return new WaitForSeconds(delay);
		}
        yield return new WaitForSeconds(textpause);
        if (isButton)
        {
            button.SetActive(true);

        }
        for (int i = fullText.Length; i >= 0; i--)
        {
            currentText = fullText.Substring(0, i);
            this.GetComponent<Text>().text = currentText;
            yield return new WaitForSeconds(0.01f);
        }

    }
}
