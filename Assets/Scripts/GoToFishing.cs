﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GoToFishing : MonoBehaviour {

    public Text spacebarText;
    public Text enterText;

    private void Update()
    {   if (spacebarText.enabled)
        {
            if (Input.GetKeyDown("space"))
            {
                Debug.Log(Data.firstChallengeAccepted);
                Debug.Log(Data.secondChallengeAccepted);
                Debug.Log(Data.thirdChallengeAccepted);
                Data.sceneChange = true;
                SceneManager.LoadScene("Kalastamine");
            }
        }

        /*else if (enterText.enabled == true)
            Debug.Log("enteririerier");
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                Debug.Log("VAJUTAB");
                SceneManager.LoadScene("TimedFishing");
            }
        }*/

    }
}
