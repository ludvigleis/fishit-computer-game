﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MiniGame : MonoBehaviour {
    public List<GameObject> CirclePrefab;

    private List<Vector3> addedCirclesPos = new List<Vector3>();
    private List<GameObject> added = new List<GameObject>();
    private float lastingTime = 6f;

    FishingUI UI;
    //bools for checking if previous circle is Destroyed
    bool previousDestroyed = false;
    bool previousDestroyed1 = false;
    bool previousDestroyed2 = false;
    bool previousDestroyed3 = false;
    bool previousDestroyed4 = false;
    bool previousDestroyed5 = false;
    bool previousDestroyed6 = false;
    // Use this for initialization
    void Start () {
        UI = FindObjectOfType<FishingUI>();
        UI.timerText.gameObject.SetActive(true);
        UI.SetTimerText(lastingTime);

        Vector3 cameraPos = Camera.main.WorldToViewportPoint(transform.position);
        foreach (GameObject circle in CirclePrefab){
            if (addedCirclesPos == null)
            {
                Vector3 ItemPos = new Vector3(
                                Random.Range(cameraPos.x - 6f, cameraPos.x + 6f),
                                Random.Range(cameraPos.x - 3f, cameraPos.x + 3f),
                                -1f);
                GameObject circa = Instantiate(circle, ItemPos, Quaternion.identity);
                circa.transform.parent = this.gameObject.transform;
                addedCirclesPos.Add(ItemPos);
                added.Add(circa);
            } else
            {
                Vector3 ItemPos;
                ItemPos = createVector(cameraPos);

                foreach (Vector3 vec in addedCirclesPos)
                {
                    if (Mathf.Abs(ItemPos.x - vec.x) <= 1f)
                    {
                        ItemPos = createVector(cameraPos);
                    }
                    else if (Mathf.Abs(ItemPos.y - vec.y) <= 1f)
                    {
                        ItemPos = createVector(cameraPos);
                    }
                }
                GameObject circa = Instantiate(circle, ItemPos, Quaternion.identity);
                circa.transform.parent = this.gameObject.transform;
                addedCirclesPos.Add(ItemPos);
                added.Add(circa);
            }
            
        }
	}
	// Update is called once per frame
	void Update () {
        if (UI.isBackbackActive || UI.isChallengeLost || UI.isChallengeWon)
        {
            UI.timerText.gameObject.SetActive(false);
            Destroy(this.gameObject);
        }
        lastingTime -= Time.deltaTime;
        UI.SetTimerText(lastingTime);

        if (lastingTime < 0)
        {
            UI.timerText.gameObject.SetActive(false);
            Destroy(this.gameObject);

            UI.fishLostPanel.gameObject.SetActive(true);
            UI.isLosePanelActive = true;
            //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        if (previousDestroyed6 == true)
        {
            UI.timerText.gameObject.SetActive(false);
            Destroy(this.gameObject);

            UI.fishCaughtPanel.gameObject.SetActive(true);
            UI.isWinPanelActive = true;
        }
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.tag == "Circle1")
                {
                    Destroy(added[0].gameObject);
                    previousDestroyed = true;
                }
                if (hit.transform.tag == "Circle2" && previousDestroyed == true)
                {
                    Destroy(added[1].gameObject);
                    previousDestroyed2 = true;
                }
                if (hit.transform.tag == "Circle3" && previousDestroyed2 == true)
                {
                    Destroy(added[2].gameObject);
                    previousDestroyed3 = true;
                }
                if (hit.transform.tag == "Circle4" && previousDestroyed3 == true)
                {
                    Destroy(added[3].gameObject);
                    previousDestroyed4 = true;
                }
                if (hit.transform.tag == "Circle5" && previousDestroyed4 == true)
                {
                    Destroy(added[4].gameObject);
                    previousDestroyed5 = true;
                }
                if (hit.transform.tag == "Circle6" && previousDestroyed5 == true)
                {
                    Destroy(added[5].gameObject);
                    previousDestroyed6 = true;
                }
            }
        }
    }

    void mouseClick()
    {
        
    }

    Vector3 createVector(Vector3 cameraPos)
    {
        Vector3 ItemPos = new Vector3(
                                Random.Range(cameraPos.x - 6f, cameraPos.x + 6f),
                                Random.Range(cameraPos.x - 3f, cameraPos.x + 3f),
                                -1f);
        return ItemPos;
    }
}
