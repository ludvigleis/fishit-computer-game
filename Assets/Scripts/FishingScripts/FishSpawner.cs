﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishSpawner : MonoBehaviour {

    public List<FishMovement> fishPreFabs;

    FishingUI UI;

    private float time = 0f;
	// Use this for initialization
	void Start () {
        UI = FindObjectOfType<FishingUI>();
    }
	
	// Update is called once per frame
	void Update () {
        if (!UI.isBackbackActive && !UI.isLosePanelActive && !UI.isWinPanelActive && !UI.isChallengeLost)
        {
            time += Time.deltaTime;

            if (time >= Random.Range(2f, 10f))
            {
                FishMovement preFab = fishPreFabs[Random.Range(0, fishPreFabs.Count)];
                transform.position = new Vector3(transform.position.x, Random.Range(0.7f, -4.0f), transform.position.z);
                Instantiate(preFab, transform.position, Quaternion.identity);
                time = 0f;
            }
        } 
        
	}
     
}
