﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FishingUI : MonoBehaviour {

    public Button changeSceneButton;
    public Text timerText;
    public Text FishName;
    public Text FishWeight;

    public Text challangetext;

    public GameObject fishLostPanel;
    public GameObject fishCaughtPanel;

    public AudioSource sound;
    public AudioClip[] sounds;


    public Text GlobalTimerText;
    private float globaltimerfloat;
    public GameObject ChallengeLostPanel;
    public GameObject ChallengeWonPanel;
    public Button ReturnToLakeButton;
    public Button ReturnToLakeButtonWon;

    public bool isLosePanelActive = false;
    public bool isWinPanelActive = false;
    public bool isChallengeLost = false;
    public bool isChallengeWon = false;

    public Button backpackButton;
    public GameObject backpack;

    public Text RoachText;
    public Text PikeText;
    public Text PerchText;
    public Text SalmonText;
    public Text RuffeText;

    public Text RoachWeight;
    public Text PikeWeight;
    public Text PerchWeight;
    public Text SalmonWeight;
    public Text RuffeWeight;

    private int RoachCount;
    private int PikeCount;
    private int PerchCount;
    private int SalmonCount;
    private int RuffeCount;

    private float RoachWeightCount;
    private float PikeWeightCount;
    private float PerchWeightCount;
    private float SalmonWeightCount;
    private float RuffeWeightCount;

    public bool isBackbackActive = false;

    public Text totalGold;

    public GameObject boatSkin;
    // Use this for initialization
    void Start () {
        changeSceneButton.onClick.AddListener(changeScene);
        backpackButton.onClick.AddListener(backPackClicked);
        ReturnToLakeButton.onClick.AddListener(changeScene);
        ReturnToLakeButtonWon.onClick.AddListener(changeScene);

        if (!(Data.boatSkin == null)){
            boatSkin.GetComponent<SpriteRenderer>().sprite = Data.boatSkin;
            boatSkin.transform.localPosition = new Vector3(0.1f, 1.2f, 2f);
            boatSkin.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
        }

        sound = GetComponent<AudioSource>();
        challangetext = GetComponent<Text>();

        if (Data.sceneChange==true && Data.secondChallengeAccepted == true)
        {
            globaltimerfloat = 180f;
            GlobalTimerText.gameObject.SetActive(true);



        }
        
        if (Data.sceneChange == true && Data.thirdChallengeAccepted == true)
        {
            globaltimerfloat = 60f;
            GlobalTimerText.gameObject.SetActive(true);
           
        }
        setTotalGold();
    }
	
	// Update is called once per frame
	void Update () {
        int totalfishCount = (RoachCount + SalmonCount + RuffeCount + PerchCount + PikeCount);
        if (!Data.secondChallengeCompleted || !Data.thirdChallengeCompleted)
        {
            globaltimerfloat -= Time.deltaTime;
            globaltimerfloat = Mathf.Round(globaltimerfloat * 100f) / 100f;
        }
        if (globaltimerfloat < 0)
        {
            isChallengeLost = true;
            GlobalTimerText.gameObject.SetActive(false);
            ChallengeLostPanel.SetActive(true);
            backpackButton.gameObject.SetActive(false);
            changeSceneButton.gameObject.SetActive(false);
        }
        //Catch 3 perch
        else if (Data.thirdChallengeAccepted && totalfishCount == 1)
        {  
            isChallengeWon = true;
            GlobalTimerText.gameObject.SetActive(false);
            ChallengeWonPanel.SetActive(true);
            backpackButton.gameObject.SetActive(false);
            changeSceneButton.gameObject.SetActive(false);

            //Data.playerGold += 300;
            //setTotalGold();
            Data.thirdChallengeCompleted = true;
        }
        //catch 10 salmon
        else if (Data.secondChallengeAccepted && SalmonCount == 1)
        {
            isChallengeWon = true;
            GlobalTimerText.gameObject.SetActive(false);
            ChallengeWonPanel.SetActive(true);
            backpackButton.gameObject.SetActive(false);
            changeSceneButton.gameObject.SetActive(false);

            //Data.playerGold += 500;
            //setTotalGold();
            Data.secondChallengeCompleted = true;
        }
        else if (Data.firstChallengeAccepted && totalfishCount == 10)
        {
            isChallengeWon = true;
            GlobalTimerText.gameObject.SetActive(false);
            ChallengeWonPanel.SetActive(true);
            backpackButton.gameObject.SetActive(false);
            changeSceneButton.gameObject.SetActive(false);

            //Data.playerGold += 200;
            //setTotalGold();
            Data.firstChallengeCompleted = true;
        }
        else
        {
            setGlobalTimerText(globaltimerfloat);
        }
	}

    void setTotalGold()
    {
        totalGold.text = ("Total gold: " + Data.playerGold);
    }

    void setGlobalTimerText(float time)
    {
        GlobalTimerText.text = ("Challenge timer: " + time.ToString() + "s");
    }

    void changeScene()
    {   
        if (Data.thirdChallengeCompleted && Data.thirdChallengeAccepted)
        {
            Data.playerGold += 300;
            setTotalGold();
        }
        //catch 10 salmon
        else if (Data.secondChallengeCompleted && Data.secondChallengeAccepted)
        {
            Data.playerGold += 500;
            setTotalGold();
        }
        else if (Data.firstChallengeCompleted && Data.firstChallengeAccepted)
        {
            Data.playerGold += 200;
            setTotalGold();
        }
        SceneManager.LoadScene("SampleScene");
    }
    public void SetTimerText(float timer)
    {
        timerText.text = ("Timer: " + timer.ToString());
    }
    public void SetCaughtFishText(string text)
    {
        FishName.text = ("Fish Caught: " + text);
       
    }
    public void SetCaughtFishWeight(float weight)
    {
        FishWeight.text = ("Fish Weight: " + weight.ToString() + "g");
       
    }


    public void backPackClicked()
    {
        if (!isBackbackActive)
        {
            backpack.SetActive(true);
            isBackbackActive = true;
        } else
        {
            backpack.SetActive(false);
            isBackbackActive = false;
        }
    }
    public void AddFishCount(string fishTag, float weight)
    {
        sound.clip = sounds[Random.Range(0, sounds.Length)];
        sound.Play();
        if (fishTag == "Ruffe")
        {
            RuffeCount += 1;
            RuffeText.text = RuffeCount.ToString();

            RuffeWeightCount += weight;
            RuffeWeight.text = RuffeWeightCount.ToString();
        }
        if (fishTag == "Roach")
        {
            RoachCount += 1;
            RoachText.text = RoachCount.ToString();
            RoachWeightCount += weight;
            RoachWeight.text = RoachWeightCount.ToString();
        }
        if (fishTag == "Pike")
        {
            PikeCount += 1;
            PikeText.text = PikeCount.ToString();

            PikeWeightCount += weight;
            PikeWeight.text = PikeWeightCount.ToString();
        }
        if (fishTag == "Salmon")
        {
            SalmonCount += 1;
            SalmonText.text = SalmonCount.ToString();
            SalmonWeightCount += weight;
            SalmonWeight.text = SalmonWeightCount.ToString();
        }
        if (fishTag == "Perch")
        {
            PerchCount += 1;
            PerchText.text = PerchCount.ToString();
            PerchWeightCount += weight;
            PerchWeight.text = PerchWeightCount.ToString();
        }
    }
}
