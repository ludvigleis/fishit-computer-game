﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishingHook : MonoBehaviour {
    Vector3 mousePos;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = -2.0f;

        if (mousePos.x<=2.5f && mousePos.x >= -7.5f && mousePos.y <= 0.5f)
        {
            transform.position = mousePos;
        }
    }
}
