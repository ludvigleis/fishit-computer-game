﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Line : MonoBehaviour {

    public GameObject Boat;
    public GameObject Hook;

    
    private LineRenderer line;
	// Use this for initialization
	void Start () {
        line = this.gameObject.AddComponent<LineRenderer>();
        line.material.color = Color.black;
        line.SetWidth(0.03f, 0.03f);
        line.SetVertexCount(2);
        Color black = Color.black;
        line.material.color = Color.black;
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 HookPos = Hook.transform.position;
        HookPos.y += 0.3f;
        HookPos.x += 0.1f;

        Vector3 BoatPos = Boat.transform.position;
        BoatPos.y += 0.15f;
        BoatPos.x -= 0.15f;

        line.SetPosition(0, BoatPos);
        line.SetPosition(1, HookPos);
	}
}
