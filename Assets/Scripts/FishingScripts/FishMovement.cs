﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishMovement : MonoBehaviour {

    public float Speed;
    public MiniGame minigamePrefab;
    
    private bool isHooked = false;
    private float fishWeight;
    GameObject hook;
    Line line;

    FishingUI UI;
    bool IsPanelActiveThis;

    public AudioSource sound;
    public AudioClip[] sounds;


    // Use this for initialization
    void Start () {
        line = FindObjectOfType<Line>();
        UI = FindObjectOfType<FishingUI>();
        sound = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 cameraPos = Camera.main.WorldToViewportPoint(transform.position);
        if (!isHooked)
        {
            if (UI.isBackbackActive || UI.isChallengeLost || UI.isChallengeWon)
            {
                Destroy(this.gameObject);
            }
            if (transform.position.x > cameraPos.x + 10.0f)
            {
                Destroy(this.gameObject);
            }
            transform.Translate(Vector3.right * Speed * Time.deltaTime);
        } else if (isHooked)
        {
            GameObject target = line.Boat;
            float movement = Speed/4 * Time.deltaTime;
            transform.position = new Vector3(Random.Range(transform.position.x - 0.06f, transform.position.x + 0.06f),
                Random.Range(transform.position.y - 0.06f, transform.position.y + 0.06f),
                -2.0f
                );

            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, movement);

            if (UI.isBackbackActive || UI.isChallengeLost || UI.isChallengeWon)
            {
                Destroy(this.gameObject);
                line.Hook = hook.gameObject;
                hook.GetComponent<SpriteRenderer>().enabled = true;
                hook.SetActive(true);
                isHooked = false;
                UI.fishLostPanel.gameObject.SetActive(false);
                UI.fishCaughtPanel.gameObject.SetActive(false);
                UI.isLosePanelActive = false;
                UI.isWinPanelActive = false;
            }

            if (UI.isLosePanelActive)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    line.Hook = hook.gameObject;
                    hook.GetComponent<SpriteRenderer>().enabled = true;
                    hook.SetActive(true);
                    Destroy(this.gameObject);
                    isHooked = false;
                    UI.fishLostPanel.gameObject.SetActive(false);
                    UI.isLosePanelActive = false;
                }
            }  else if (UI.isWinPanelActive)
            {
               
                if (Input.GetMouseButtonDown(0))
                {
                    UI.AddFishCount(this.gameObject.tag, fishWeight);

                    Destroy(this.gameObject);
                    line.Hook = hook.gameObject;
                    hook.GetComponent<SpriteRenderer>().enabled = true;
                    hook.SetActive(true);
                    isHooked = false;
                    UI.fishCaughtPanel.gameObject.SetActive(false);
                    UI.isWinPanelActive = false;
                }

            }


            /*if (transform.position.y >= target.transform.position.y - 0.2f) {
                if (UI.isLosePanelActive)
                    {   
                        if (Input.GetMouseButtonDown(0))
                        {
                            Destroy(this.gameObject);
                            line.Hook = hook.gameObject;
                            hook.GetComponent<SpriteRenderer>().enabled = true;
                            hook.SetActive(true);
                            isHooked = false;
                            UI.fishLostPanel.gameObject.SetActive(false);
                            UI.isLosePanelActive = false;
                        }
                    }
            else if(UI.isWinPanelActive) {
                    if (Input.GetMouseButtonDown(0))
                        {   
                            Destroy(this.gameObject);
                            line.Hook = hook.gameObject;
                            hook.GetComponent<SpriteRenderer>().enabled = true;
                            hook.SetActive(true);
                            isHooked = false;
                            UI.fishCaughtPanel.gameObject.SetActive(false);
                            UI.isWinPanelActive = false;
                            
                            
                        }
                           
                    }
            }*/
        }
        
	}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "hook")
        {
            if (!isHooked)
            {   
                hook = GameObject.Find("FishingHook");
                hook.SetActive(false);

                UI.SetCaughtFishText(this.gameObject.tag);
                fishWeight = Mathf.Round(Random.Range(15f, 1000f) * 10.0f) * 0.1f;
                UI.SetCaughtFishWeight(fishWeight);

                Instantiate(minigamePrefab, transform.position, Quaternion.identity);

                isHooked = true;
                line.Hook = this.gameObject;
                
            }
            sound.clip = sounds[Random.Range(0, sounds.Length)];
            sound.Play();

        }
    }
}
