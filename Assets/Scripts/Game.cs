﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game: MonoBehaviour {

    public AudioClipGroup sounds1;

    public AudioClipGroup sounds2;

    public AudioClipGroup sounds3;

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            sounds1.Play();
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            sounds2.Play();
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            sounds3.Play();
        }
    }
}
