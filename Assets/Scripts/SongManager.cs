﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SongManager : MonoBehaviour{


    public static SongManager instance = null;
    public static SongManager Instance
    {
        get { return instance; }
    }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance  = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }
   
     
}

