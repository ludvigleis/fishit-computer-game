﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowPanel : MonoBehaviour {

    public Button panelButton;
    public GameObject panel;
    private bool isPanel;

	// Use this for initialization
	void Start () {
        panelButton.onClick.AddListener(changePanel);
        panel.SetActive(true);
        isPanel = true;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void changePanel()
    {
        if (isPanel == true)
        {
            panel.SetActive(false);
            isPanel = false;
        }
        else
        {
            panel.SetActive(true);
            isPanel = true;
        }
       
    }
}
