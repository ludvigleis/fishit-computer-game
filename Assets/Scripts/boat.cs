﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class boat : MonoBehaviour {

    
    private Vector3 target;
    private Vector3 mousPos;

    private float turnSpeed = 2f;
    private float angle;
    private float speed = 1;

    public GameObject Shore;
    public GameObject Challange;

    public Text challangeText;
    public Text timedChallangeText;
    public Text thirdChallengeText;

    public Text startFishing;
    //public Text startTimedFishing;

    public Button goFishing;

    private Rigidbody2D rb;
    private Vector3 test;

    public AudioClipGroup rowingSound;

    public Button ShopButton;
    private bool shopbuttonopen = false;
    public GameObject ShopPanel;

    public Button boat1, boat2, boat3, boat4, boat5, boat6;

    // Use this for initialization
    void Start () {
        ShopButton.onClick.AddListener(shopClicked);
        
        
        boat1.onClick.AddListener(boat1Clicked);
        boat2.onClick.AddListener(boat2Clicked);
        boat3.onClick.AddListener(boat3Clicked);
        boat4.onClick.AddListener(boat4Clicked);
        boat5.onClick.AddListener(boat5Clicked);
        boat6.onClick.AddListener(boat6Clicked);

        updateChanges();

        Vector3 nullVector = new Vector3(0f, 0f, 0f);
        if (Data.position.Equals(nullVector))
        {
            target = transform.position;
        } else
        {
            Vector3 pos = Data.position;
            transform.position = pos;
            target = Data.position;
        }
        Debug.Log("start"+ target);
        rb = GetComponent<Rigidbody2D>();
        challangeText.enabled = false;
        timedChallangeText.enabled = false;
        thirdChallengeText.enabled = false;

        startFishing.enabled = false;
        goFishing.enabled = false;
        //startTimedFishing.enabled = false;
        if (Data.thirdChallengeCompleted)
        {
            GameObject.FindGameObjectWithTag("ThirdChallenge").SetActive(false);
        }
        if (Data.secondChallengeCompleted)
        {
            GameObject.FindGameObjectWithTag("ChallangeTimed").SetActive(false);
        }
        if (Data.firstChallengeCompleted)
        {
            GameObject.FindGameObjectWithTag("Challange").SetActive(false);
        }

    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            // Check if the mouse was clicked over a UI element
            if (EventSystem.current.IsPointerOverGameObject())
            {
                Debug.Log("Clicked on the UI");
            } else
            {
                test = Input.mousePosition;
                mousPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                speed = 1;
                target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                angle = Mathf.Atan2((target.y - transform.position.y),
                (target.x - transform.position.x)) * Mathf.Rad2Deg - 180;
                target.z = transform.position.z;
                Data.position = target;
                rb.velocity = new Vector3(0, 0, 0);
                rb.AddForce((mousPos - transform.position) * 0.4f * Time.deltaTime);
                //rb.velocity = (mousPos - transform.position) * 10 * Time.deltaTime;
                rowingSound.Play();
            }
            
        }
        //transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, angle), turnSpeed * Time.deltaTime);
       
    }
    void OnTriggerStay2D(Collider2D collision)
    {
 
        if(collision.gameObject == Shore)
        { 
            speed = 0;
        }

        if (collision.gameObject.tag == "Challange" && !Data.firstChallengeCompleted)
        {
            Data.firstChallengeAccepted = true;
            challangeText.enabled = true;
            startFishing.enabled = true;
            goFishing.enabled = true;
        }
        else if (collision.gameObject.tag == "ChallangeTimed" && !Data.secondChallengeCompleted)
        {
            Data.secondChallengeAccepted = true;
            timedChallangeText.enabled = true;
            startFishing.enabled = true;
            goFishing.enabled = true;
        }
        else if (collision.gameObject.tag == "ThirdChallenge" && !Data.thirdChallengeCompleted)
        {
            Data.thirdChallengeAccepted = true;
            thirdChallengeText.enabled = true;
            startFishing.enabled = true;
            goFishing.enabled = true;
        }


    }
    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Challange")
        {
            Data.firstChallengeAccepted = false;
            challangeText.enabled = false;
            startFishing.enabled = false;
            goFishing.enabled = false;
        }
        else if (collision.gameObject.tag == "ChallangeTimed")
        {
            Data.secondChallengeAccepted = false;
            timedChallangeText.enabled = false;
            startFishing.enabled = false;
            goFishing.enabled = false;
        }
        else if (collision.gameObject.tag == "ThirdChallenge")
        {
            Data.thirdChallengeAccepted = false;
            thirdChallengeText.enabled = false;
            startFishing.enabled = false;
            goFishing.enabled = false;
        }
    }
    public void shopClicked()
    {
        if (!shopbuttonopen)
        {
            ShopPanel.SetActive(true);
            shopbuttonopen = true;
        }
        else
        {
            ShopPanel.SetActive(false);
            shopbuttonopen = false;
        }
    }

    public void boat1Clicked()
    {
        if (!Data.boat1Owned)
        {
            Data.boat1Owned = true;
            Data.playerGold -= 200;
            Data.boatSkin = boat1.GetComponent<Image>().sprite;
            updateChanges();
        }
        else if (Data.boat1Owned && Data.boatSkin != boat1.GetComponent<Image>().sprite)
        {
            Data.boatSkin = boat1.GetComponent<Image>().sprite;
        }
    }


    //Boat2Clicked
    public void boat2Clicked()
    {
        if (!Data.boat2Owned)
        {
            Data.boat2Owned = true;
            Data.playerGold -= 300;
            Data.boatSkin = boat2.GetComponent<Image>().sprite;
            updateChanges();
        }
        else if(Data.boat2Owned && Data.boatSkin != boat2.GetComponent<Image>().sprite)
        {
            Data.boatSkin = boat2.GetComponent<Image>().sprite;
        }
    }
    //Boat3Clicked
    public void boat3Clicked()
    {
        if (!Data.boat3Owned)
        {
            Data.boat3Owned = true;
            Data.playerGold -= 400;
            Data.boatSkin = boat3.GetComponent<Image>().sprite;
            updateChanges();
        }
        else
        {

        }
    }
    //Boat4Clicked
    public void boat4Clicked()
    {
        if (!Data.boat4Owned)
        {
            Data.boat4Owned = true;
            Data.playerGold -= 500;
            Data.boatSkin = boat4.GetComponent<Image>().sprite;
            updateChanges();
        }
        else
        {

        }
    }
    //Boat5Clicked
    public void boat5Clicked()
    {
        if (!Data.boat5Owned)
        {
            Data.boat5Owned = true;
            Data.playerGold -= 700;
            Data.boatSkin = boat5.GetComponent<Image>().sprite;
            updateChanges();
        }
        else
        {

        }
    }
    //Boat6Clicked
    public void boat6Clicked()
    {
        if (!Data.boat6Owned)
        {
            Data.boat6Owned = true;
            Data.playerGold -= 800;
            Data.boatSkin = boat6.GetComponent<Image>().sprite;
            updateChanges();
        }
        else
        {

        }
    }

    public void updateChanges()
    {
        if (Data.boat1Owned)
        {
            boat1.interactable = true;
            boat1.GetComponentInChildren<Text>().text = "Boat1 owned";
        } else if(Data.playerGold >= 200)
        {
            boat1.interactable = true;
        } else
        {
            boat1.interactable = false;
        }


        if (Data.boat2Owned)
        {
            boat2.interactable = true;
            boat2.GetComponentInChildren<Text>().text = "Boat2 owned";
        }
        else if (Data.playerGold >= 300)
        {
            boat2.interactable = true;
        }
        else
        {
            boat2.interactable = false;
        }

        if (Data.boat3Owned)
        {
            boat3.interactable = true;
            boat3.GetComponentInChildren<Text>().text = "Boat3 owned";
        }
        else if (Data.playerGold >= 400)
        {
            boat3.interactable = true;
        }
        else
        {
            boat3.interactable = false;
        }

        if (Data.boat4Owned)
        {
            boat4.interactable = true;
            boat4.GetComponentInChildren<Text>().text = "Boat4 owned";
        }
        else if (Data.playerGold >= 500)
        {
            boat4.interactable = true;
        }
        else
        {
            boat4.interactable = false;
        }

        //Boat5
        if (Data.boat5Owned)
        {
            boat5.interactable = true;
            boat5.GetComponentInChildren<Text>().text = "Boat5 owned";
        }
        else if (Data.playerGold >= 700)
        {
            boat5.interactable = true;
        }
        else
        {
            boat5.interactable = false;
        }

        //Boat6
        if (Data.boat6Owned)
        {
            boat6.interactable = true;
            boat6.GetComponentInChildren<Text>().text = "Boat6 owned";
        }
        else if (Data.playerGold >= 800)
        {
            boat6.interactable = true;
        }
        else
        {
            boat6.interactable = false;
        }

    }
}

